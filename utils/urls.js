var host = "https://api.douxunyun.com/apps"
// let host="http://101.132.144.73:8080"
// 所有数据请求服务器接口
const globalUrls ={
  serverUrl : host,
  // 获取首页咨询列表
  goodslist: host +"/goodslist",  
  defaultContent:"application/json",
  // 登录判断是否存在openId
  onLoginUrl: host +"/code2Session",
  goodsinfo:host+"/goodsinfo",
  addorder:host+"/addorder",
  insertUser:host+"/insertUser",
  addPic:host+"/addPic",
  get_message:host+"/get_message",
  send_message:host+"/send_message",
  buy:host+"/buy",
  getBoughtInfoByUserId:host+"/getBoughtInfoByUserId",
  getSign:host+"/getSign",
  adetcontacts:host+"/adetcontacts",
  mycontacts:host+"​/mycontacts",
  myorder:host+"/myorder",
  mymyorderinfo:host+"/myorderinfo",
  mywallet:host+"/mywallet",
  storeapply:host+"/storeapply",
  searchgoods:host+"/searchgoods",
  setuserinfo:host+"/setuserinfo",
  ordercomment:host+"/ordercomment",
  getcoupon:host+"/getcoupon",
  mycoupon:host+"/mycoupon",
  addcomment:host+"/addcomment",
  createvideo:host+"/createvideo"
}
export {globalUrls} 