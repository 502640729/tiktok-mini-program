//app.js
import {
  globalUrls
} from "/utils/urls.js"
App({
  onLaunch: function () {
    this.onLogin()

    // 获取系统状态栏信息
    tt.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = tt.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else this.globalData.CustomBar = e.statusBarHeight + 50;

      }
    })
  },
  onLogin: function () {
    let that = this
    tt.showLoading({
      title: '请稍后',
    })
    tt.login({
      success: function (yes) {
        tt.request({
          url: globalUrls.onLoginUrl + "?code=" + yes.code,
          header: {
            'content-type': "application/json",
          },
          method: "POST",
          success(res) {
            if (res.statusCode != 200 || res.success == false) {
              tt.showToast({
                title: '连接失败,请检查网络连接',
                icon: 'none'
              })
              return
            }
            let data = res.data.data;
            that.globalData.userInfo = data.token;
            that.globalData.openid = data.openid
            if (that.userIdReadyCallback) {
              that.userIdReadyCallback(res)
            }
            tt.getSetting({
              success(res) {
                tt.authorize({
                  scope: "scope.userInfo",
                  success() {
                    tt.getUserInfo({
                      success: (res) => {
                        console.log(res)
                        that.globalData.info = res.userInfo
                        that.globalData.haveInfo = true;
                        let data = "?nickName=" + res.userInfo.nickName + "&avatarUrl=" + res.userInfo.avatarUrl;
                        tt.request({
                          url: globalUrls.setuserinfo + data,
                          header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + data.token},
                          method: "POST",
                          success(res) {
                          }, fail() {
                            tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
                          }
                        })
                        
                      },
                    })
                  }
                });
              },
            });

          },
          fail: (e) => {

            tt.showToast({
              title: '连接失败,请检查网络连接',
              icon: 'none'
            })
          },
          complete: () => {
            tt.hideLoading()
          }
        })
      },
      fail(e) {

        tt.showToast({
          title: '请检查网络连接',
          icon: 'none'
        })
      }
    })
    tt.getLocation({
      success(res) {
        let location = {};
        location["jd"] = res.longitude;
        location["wd"] = res.latitude;
        that.globalData.location = location;
      },
      fail(res) {
        console.log(`getLocation调用失败`);
      },
    });
  },
  globalData: {
    userInfo: "",
    servUrl: globalUrls.serverUrl,
    location: { "jd": 119, "wd": 39 },
    openid: "",
    info: {},
    haveInfo: false
  }
})