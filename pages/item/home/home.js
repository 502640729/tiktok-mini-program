import {
  globalUrls
} from "../../../utils/urls.js"
let app = getApp()
Page({
  data: {
    CustomBar: app.globalData.CustomBar,
    Tab: ["最新开抢", "当前热门", "离我最近", "超级低价"],
    subtab: ["甄选优质好物", "推荐最高人气", "发现精彩周边", "美好现在开始"],
    TabCur: 0,
    scrollLeft: 0,
    fs: "",
    pn: 1,
    ColorList: app.globalData.ColorList,
    color: 'red',
    loading: "",
    have_next: true,
    item: [],
    tag: []
  },
  onLoad() {
    let that = this;
    if (app.globalData.userInfo) {
      this.getTagList()
      this.getItemList(0)
    } else{
      app.userIdReadyCallback = res => {
        this.getTagList()
        this.getItemList(0)
      }
    }
    setTimeout(function () {
      that.setData({
        loading: true
      })
    }, 500)
  },
  onShow(){
    if (app.globalData.userInfo) {
      this.getTagList()
      this.getItemList(0)
    } else{
      app.userIdReadyCallback = res => {
        this.getTagList()
        this.getItemList(0)
      }
    }
  },
  search: function () {
    tt.navigateTo({
      url: '/pages/item/search/search?type=搜索',
    })
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60,
    })
    this.getItemList(0)
  },
  jump: function () {
    tt.showDouyinOpenAuth({
      scopes: {
        "video.create": 0,
        user_info: 1,
      },
      success: (res) => {
        console.log("success", res);
      },
      fail: (res) => {
        console.log("fail", res);
      },
      complete: (res) => {
        console.log("complete", res);
      },
    });
  },
  onShareAppMessage: function (shareOption) {
    switch (shareOption.channel) {
      case 'video':
        return {
          extra: {
            // 注意，只有小程序使用button组件触发分享时，会有target属性
            videoPath: shareOption.target.dataset.path
          }
        };
    }
  },
  getItemList(status) { //0 全部刷新 1拼接
    let that = this;
    let pn = that.data.pn
    if (status == 1) {
      pn += 1
    } else {
      pn = 1
    }
    that.setData({
      pn: pn
    })
    let data = "?type=" + (parseInt(that.data.TabCur) + 1) + "&pagesize=6&lon=" + app.globalData.location.jd + "&lat=" + app.globalData.location.wd + "&pageindex=" + pn;
    tt.request({
      url: globalUrls.goodslist + data,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let datas = res.data.data
        for (let i = 0; i < datas.length; i++) {
          datas[i]["per"] = parseInt((parseInt(datas[i].sales) / (parseInt(datas[i].sales) + parseInt(datas[i].stock))) * 100)
        }
        let item = []
        if (status == 1) {
          item = that.data.item.concat(res.data.data)
        } else {
          item = res.data.data
        }
        that.setData({
          item: item,
        })
      }, fail() {
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  getTagList: function () {
    let that = this;
    tt.request({
      url: "https://api.douxunyun.com/apps/classification",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let ds = res.data.data
        that.setData({
          tag: ds
        })
      }, fail() {
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.getItemList(0);
    setTimeout(function () {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 500);

  },
  onReachBottom: function (e) {
    wx.showNavigationBarLoading();
    let that = this
    setTimeout(function () {
      wx.hideNavigationBarLoading();
      that.getItemList(1);
    }, 500);

  },


})