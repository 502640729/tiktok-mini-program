import {
  globalUrls
} from "../../../utils/urls.js"
const app = getApp();
Page({
  data: {
    remark: '',
    uid: "",
    modalName: "",
    name: "",
    phone: "",
    contact: [],
    picker: ["新增联系人"],
    count: 1,
    selectuser: null,
    item: [],
    imgs: "",
    isDefault: false,
    index: -1,
    price: 0,
    parentid:0,
    dicountPicker:[""],
    dicountIndex:-1,
    discount:[]
  },
  onLoad: function (options) {
    this.mycontacts()
    let uid = options.uid;
    let parentid=options.parentid;
    this.getItem(uid)
    this.mycoupon();
    this.setData({
      uid: uid,
      price: this.data.item.price,
      parentid:parentid
    })
  },
  mycoupon:function(){
    let that = this;
    tt.request({
      url: globalUrls.mycoupon + "?status=" + 0,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '网络异常',
            icon: "error"
          })
          return;
        }
        let discount=[]
        for(let i=0;i<res.data.data.length;i++){
          discount.push(res.data.data[i].name)
        }
        console.log(discount)
        that.setData({
          dicountPicker:res.data.data,
          discount:discount
        })
      }
    })
  },
  getItem: function (uid) {
    let that = this;
    tt.request({
      url: globalUrls.goodsinfo + "?uid=" + uid,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "error"
          })
          return;
        }
        that.setData({
          item: res.data.data,
          imgs: res.data.data.coverurl,
          price: res.data.data.price.toFixed(2)
        })
      }, fail() {
        tt.showToast({
          title: '网络异常',
        })
      }
    })
  },
  mycontacts: function () {
    let that = this;
    tt.request({
      url: "https://api.douxunyun.com/apps/mycontacts",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
          return;
        }
        let datas = res.data.data;
        let pk = ["新增联系人"]
        for (let i = 0; i < datas.length; i++) {
          pk.push(datas[i].name)
          if (datas[i].isdefault == 1) {
            that.setData({
              index: i,
              name: datas[i].name,
              phone: datas[i].tel,
              selectuser: datas[i].uid,
              isDefault: true
            })
          }
        }
        that.setData({
          picker: pk,
          contact: datas
        })
      }, fail() {
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  buyIt: function () {
    if (this.data.selectuser == null && this.data.picker.length != 1) {
      tt.showToast({
        title: '请选择用户',
        icon: "none"
      });
      return;
    }
    if (this.data.picker.length == 1 || this.data.index == -1) {
      this.addDetail();
    } else {
      this.addOrder(this.data.selectuser)
    }
  },
  addDetail: function () {
    let that = this;
    let dt = "?name=" + this.data.name + "&tel=" + this.data.phone + "&isdefault=" + (this.data.isDefault == true ? 1 : 0)
    tt.request({
      url: globalUrls.adetcontacts + dt,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
          return;
        }
        that.addOrder(res.data.data)
      }
    })
  },
  PickerChange(e) {
    let index = e.detail.value;
    if (index == 0) {
      this.setData({
        name: "",
        phone: "",
        index: -1
      })
      return;
    }
    index = parseInt(index) - 1;
    this.setData({
      index: index,
      selectuser: this.data.contact[index].uid,
      name: this.data.contact[index].name,
      phone: this.data.contact[index].tel,
      isDefault: (this.data.contact[index].isdefault == 0 ? false : true)
    })
  },
  DiscountPickerChange(e) {
    let index = e.detail.value;
    let price=index>=0?(this.data.price-this.data.dicountPicker[index].money):this.data.price
    this.setData({
      dicountIndex: index,
      price:price
    })
  },
  addOrder: function (userid) {
    let ds = { "name": this.data.name, "tel": this.data.phone, "isdefault": 0 }
    if (this.data.picker.length == 0)
      ds.isdefault = 1;
    let data = "?goodsuid=" + this.data.uid + "&goodscount=" + this.data.count + "&remark=" + encodeURI(this.data.remark) + "&contactuid=" + userid
    if(this.data.parentid!=0&&this.data.parentid!=null){
      data=data+"&uid="+this.data.parentid
    }
    if(this.data.dicountIndex>=0)
      data+="&couponuid="+this.data.dicountPicker[this.data.dicountIndex].uid
    tt.request({
      url: globalUrls.addorder + data,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: (res) => {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "error"
          })
          return;
        }
        tt.pay({
          service: 5,
          orderInfo: res.data.data.orderinfo,
          success(res) {
            if (res.code === 0) {
              tt.showToast({
                title: '支付成功',
                icon: "success"
              });
              setTimeout(function () {
                tt.navigateTo({
                  url: '/pages/item/payover/payover'
                });
              }, 500)
            } else {
              tt.showToast({
                title: '支付失败',
                icon: "success"
              });
            }
          },
          fail(res) {
            tt.showToast({
              title: '支付失败',
              icon: "none"
            });
          },
        });
      }, fail() {
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    });
  },
  remark(e) {
    this.setData({
      remark: e.detail.value
    })
  },
  nameInput: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  up: function () {
    this.setData({
      count: this.data.count + 1
    })
    this.setData({
      price: parseFloat(this.data.item.price * this.data.count).toFixed(2)
    })
  },
  down: function () {
    this.setData({
      count: this.data.count > 1 ? (this.data.count - 1) : 1
    })
    this.setData({
      price: parseFloat(this.data.item.price * this.data.count).toFixed(2)
    })
  },
  selectDefault: function (e) {
    if (e.detail.value == true) {
      let datas = this.data.contact
      for (let i = 0; i < datas; i++) {
        if (datas[i].isdefault == 1) {
          datas[i].isdefault = 0;
        }
      }
      datas[this.data.index].isdefault = 1;
      console.log(this.data.contact[this.data.index])
      this.editDetail(this.data.contact[this.data.index])
      this.setData({
        contact: datas,
        isDefault: true
      })
    } else {
      let contact = this.data.contact
      contact[this.data.index].isdefault = 0;
      console.log(this.data.contact[this.data.index])
      this.editDetail(this.data.contact[this.data.index])
      this.setData({
        contact: contact,
        isDefault: false
      })

    }
  },
  DiscountPickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },
  editDetail: function (data) {
    let that = this;
    let dt = "?name=" + data.name + "&tel=" + data.tel + "&isdefault=" + data.isdefault + "&uid=" + data.uid
    console.log(dt)
    tt.request({
      url: globalUrls.adetcontacts + dt,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
          return;
        }
      }, fail() {
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
})