import {
  globalUrls
} from "../../../utils/urls.js"
let app = getApp();
Page({
  data: {
    item: [],
    servUrl: globalUrls.serverUrl,
    typeArr: ["全部分类", "火锅", "电影"],
    TabCur: 0,
    types: "",
    searchContent: "",
    item: [],
    ColorList: app.globalData.ColorList,
    color: 'red',
    pn: 1

  },
  onLoad: function (options) {
    let types = options.type;
    let that = this;
    this.search(0);
    this.setData({
      types: types
    })
    setTimeout(function () {
      that.setData({
        loading: true
      })
    }, 500)
  },
  searchInput: function (e) {
    this.setData({
      searchContent: e.detail.value
    })
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
    })
  },
  search: function (status) {
    let that = this;
    let pn = that.data.pn
    if (status == 1) {
      pn += 1
    } else {
      pn = 1
    }
    that.setData({
      pn: pn
    })
    tt.request({
      url: globalUrls.searchgoods + "?keywrod=" +encodeURI(this.data.searchContent)  + "&pagesize=6&pageindex="+pn+"&classification=0",
      header: {'content-type': globalUrls.defaultContent,"Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let datas = res.data.data
        for (let i = 0; i < datas.length; i++) {
          datas[i]["per"] = parseInt((parseInt(datas[i].sales) / (parseInt(datas[i].sales) + parseInt(datas[i].stock))) * 100)
        }
        that.setData({
          item: datas
        })
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  getTagList:function(){
    let that=this;
    tt.request({
      url: "https://api.douxunyun.com/apps/classification",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let ds=res.data.data
        ds.push({"id":0,"title":"all"})
        that.setData({
          typeArr:ds
        })
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.search(0);
    setTimeout(function () {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 500);

  },
  onReachBottom: function (e) {
    wx.showNavigationBarLoading();
    let that = this
    setTimeout(function () {
      wx.hideNavigationBarLoading();
      that.search(1);
    }, 500);

  },

})