import { globalUrls } from "../../../utils/urls.js"
let app = getApp()
Page({
  data: {
    item: {},
    uid: 0,
    servUrl: globalUrls.serverUrl,
    imgs: "",
    uid: 0,
    parentid: 0,
    selected: 0,
    pn: 1,
    dStatus: "立即领取"
  },
  onLoad: function (options) {
  },
  onShareAppMessage: function (res) {
    let qid=this.data.uid
    return {
        title: this.data.item.title,
        path: 'pages/item/goods/goods?uid='+qid,
        success: function (res) {
           tt.showToast({
             title: '发送成功',
             icon:'success'
           })
        },
        fail: function (res) {
            //转发失败
        }
    }

},
  onShow: function (options) {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    options = currentPage.options
    let uid = options.orderid;
    let parentid = options.parentid;
    this.setData({
      uid: uid,
      parentid: parentid
    })
    this.ordercomment();
    this.getItem();
  },
  buys: function () {
    let that = this;
    let url = '/pages/item/paypage/paypage?uid=' + that.data.uid;
    if (this.data.parentid && this.data.parentid != null && this.data.parentid != 0)
      url += "&parentid=" + this.data.parentid
    if (this.data.item.buyinstruction == "") {
      tt.navigateTo({
        url: url
      });
    } else {
      wx.showModal({
        title: "购买须知",
        content: this.data.item.buyinstruction,
        showCancel: false,
        success(res) {
          tt.navigateTo({
            url: url
          });
        }
      });
    }
  },

  openLocation() {
    let lat = this.data.item.lat == "" ? 39.54 : this.data.item.lat
    let lon = this.data.item.lon == "" ? 116.23 : this.data.item.lon
    console.log(lat + " " + lon)
    tt.openLocation({
      latitude: lat,
      longitude: lon,
      scale: 18,
      success() {
        console.log("打开地图成功");
      },
      fail(res) {
        console.log("打开地图失败:", res.errMsg);
      },
    });
  },

  ordercomment: function () {
    let that = this;
    let pn = that.data.pn
    pn += 1
    that.setData({
      pn: pn
    })
    tt.request({
      url: globalUrls.ordercomment + "?uid=" + that.data.uid + "&pagesize=6&pageindex=" + pn,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {

        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
        }
        that.setData({
          itemComment: res.data.data
        })
        return;
      }
    })
  },
  getDisCount: function (e) {
    let that = this;
    let uid = e.currentTarget.dataset.uid
    tt.request({
      url: globalUrls.getcoupon + "?uid=" + uid,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          if (res.data.msg == "你已经领取过了")
            that.setData({
              "dStatus": "已领取"
            })
          tt.showToast({
            title: res.data.msg,
            icon: "none"
          })
          return;
        }
        tt.showToast({
          title: "领取成功",
          icon: "none"
        })

      }
    })
  },

  getItem: function () {
    let that = this;
    tt.request({
      url: globalUrls.goodsinfo + "?uid=" + that.data.uid,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
          return;
        }
        let datas = res.data.data;
        console.log(datas)
        that.setData({
          item: datas,
          imgs: res.data.data.coverurl
        })
      }, fail() {
        tt.showToast({
          title: '网络异常',
        })
      }
    })
  },
  onReachBottom: function (e) {
    wx.showNavigationBarLoading();
    let that = this
    if (this.data.selected == 2) {
      setTimeout(function () {
        wx.hideNavigationBarLoading();
        that.ordercomment();
      }, 500);
    }


  },
  onPullDownRefresh: function () {
    return;
  },
  toHome: function () {
    tt.navigateTo({
      url: '/pages/item/home/home' // 指定页面的url
    });
  },
  call() {
    tt.makePhoneCall({
      phoneNumber: this.data.item.tel,
      success(res) {
      },
    });
  },

  tabSelect(e) {
    this.setData({
      selected: e.currentTarget.dataset.id,
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  defaultVideo: function () {
    let that = this;
    let url = "?uid=" + that.data.uid;
    if (this.data.parentid && this.data.parentid != null && this.data.parentid != 0)
      url += "&parentid=" + this.data.parentid
    tt.request({
      url: globalUrls.createvideo,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "none"
          })
          return;
        }
        let datas = res.data.data;
        console.log(datas)
        that.setData({
          item: datas,
          imgs: res.data.data.coverurl
        })
      }, fail() {
        tt.showToast({
          title: '网络异常',
        })
      }
    })
  }
})