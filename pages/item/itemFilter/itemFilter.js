import { globalUrls } from "../../../utils/urls.js"
let app = getApp()
Page({
  options: {
    addGlobalClass: true,
  },
  data: {
    haveUserInfo: false,
    userInfo: {},
    servUrl: globalUrls.serverUrl,
    typeArr: ["全部分类", "火锅", "电影"],
    TabCur: 0,
    item: [],
    tag: "",
    pn: 1,
    ColorList: app.globalData.ColorList,
    color:'red',
    searchContent:""
  },
  onLoad: function (options) {
    let that=this;
    let tag = options.tag;
    this.setData({
      TabCur: parseInt(tag)-2
    })
    this.getTagList();
    this.search(0,tag)
    setTimeout(function () {
      that.setData({
        loading: true
      })
    }, 500);

  },
  initSearch:function(){
    this.search(0,0)
  },
  getSearch:function(){
    this.search(0,this.data.typeArr[this.data.TabCur].id)
  },
  search: function (status,cls) {
    let that = this;
    let pn = that.data.pn
    if (status == 1) {
      pn += 1
    } else {
      pn = 1
    }
    that.setData({
      pn: pn
    })
    tt.request({
      url: globalUrls.searchgoods + "?keywrod=" +encodeURI(this.data.searchContent)  + "&pagesize=6&pageindex="+pn+"&classification="+cls,
      header: {'content-type': globalUrls.defaultContent,"Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let datas = res.data.data
        for (let i = 0; i < datas.length; i++) {
          datas[i]["per"] = parseInt((parseInt(datas[i].sales) / (parseInt(datas[i].sales) + parseInt(datas[i].stock))) * 100)
        }
        that.setData({
          item: datas
        })
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      searchContent:""
    })
    this.search(0,this.data.typeArr[e.currentTarget.dataset.id].id)
  },
  getTagList:function(){
    let that=this;
    tt.request({
      url: "https://api.douxunyun.com/apps/classification",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let ds=res.data.data
        that.setData({
          typeArr:ds
        })
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  onPullDownRefresh: function () {
      tt.showNavigationBarLoading()
      this.search(0,parseInt(this.data.TabCur)+2);
      setTimeout(function () {
        tt.hideNavigationBarLoading();
        tt.stopPullDownRefresh();
      }, 500);
    
  },
  onReachBottom: function (e) {
      tt.showNavigationBarLoading();
      let that = this
      setTimeout(function () {
        tt.hideNavigationBarLoading();
        that.search(1,parseInt(this.data.TabCur)+2);
      }, 500);
    
  },
  searchInput: function (e) {
    this.setData({
      searchContent: e.detail.value
    })
  },
})