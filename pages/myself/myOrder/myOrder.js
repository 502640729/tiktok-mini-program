
import {
  globalUrls
} from "../../../utils/urls.js"
let app = getApp()
Component({

  data: {
      Tab: ["全部订单", "待付款", "待使用"],
      indexs:[5,0,1],
      tag:5,
      scrollLeft: 0,
      item:[],
      index:0
  },
  methods: {
    onLoad: function (options) {
      let tag = options.tag;
      this.setData({
        tag:tag
      })
      this.getOrder(tag);
    },
    tabSelect(e) {
      this.setData({
        tag: e.currentTarget.dataset.id,
        scrollLeft: (e.currentTarget.dataset.id - 1) * 60
      })
      this.getOrder(e.currentTarget.dataset.id)
    },
    getOrder:function(tag){
      let that=this;
      tt.request({
        url: globalUrls.myorder+"?type="+that.data.indexs[tag] ,
        header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
        method: "POST",
        success(res) {
          let datas=res.data.data
          that.setData({
            item: datas,
          })
        }, fail() {
          tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
        }
      })
    }

}})
