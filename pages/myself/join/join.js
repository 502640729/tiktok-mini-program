const {
  globalUrls
} = require("../../../utils/urls")
let app = getApp()
Page({
  data: {
    name: "",
    phone: ""
  },
  onLoad: function (options) {

  },
  submitInfo: function () {
    if (this.data.name == "" || this.data.phone == "") {
      tt.showToast({
        title: '请输入信息',
        icon: "none"
      });
      return;
    }
    let that = this;
    let data="?name="+this.data.name+"&tel="+this.data.phone;
    tt.request({
      url: "https://api.douxunyun.com/apps/storeapply"+data,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '服务器异常',
            icon: "error"
          })
          return;
        }
        tt.navigateTo({
          url: '/pages/myself/joinover/over' // 指定页面的url
        });
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  nameInfo: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  phoneInfo: function (e) {
    console.log(e)
    this.setData({
      phone: e.detail.value
    })
  }

})