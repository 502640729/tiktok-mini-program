const {
  globalUrls
} = require("../../../utils/urls")
let app = getApp()
Page({
  data: {
    ColorList: app.globalData.ColorList,
    color:'red',
  },
  onLoad: function (options) {
    let that=this;
    setTimeout(function () {
      that.setData({
        loading: true
      })
    }, 500)
    this.getItemList();
  },
  getItemList() { //0 全部刷新 1拼接
    let that = this;
    let data = "?type=2&pagesize=6&lon=" + app.globalData.location.jd + "&lat=" + app.globalData.location.wd + "&pageindex=1";
    tt.request({
      url: globalUrls.goodslist + data,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if(res.data.success==false){
          tt.showToast({
            title: '服务器异常',
            icon:"error"
          })
          return;
        }
        let datas=res.data.data
        for(let i=0;i<datas.length;i++){
          datas[i]["per"]=parseInt( (parseInt( datas[i].sales)/(parseInt(datas[i].sales)+parseInt(datas[i].stock)))*100)
        }
        that.setData({
          item: datas,
        })
      }, fail() {
        tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
      }
    })
  },
})