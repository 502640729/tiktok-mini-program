
import {
  globalUrls
} from "../../../utils/urls.js"
let app = getApp()
Page({
  data: {
    item: {},
    marker: [],
    imgList: [],
    uid: "",
    itemComment: [],
    star: 1,
    comment: "",
  },
  onLoad: function (options) {
    let uid = options.uid;
    this.setData({
      uid: uid
    })
    this.getOrderInfo(uid)
  },
  setStar: function (e) {
    let star = e.currentTarget.dataset.id;
    this.setData({
      star: star
    })
  },
  openLocation() {
    let lat=this.data.item.lat==""?39.54:this.data.item.lat
    let lon = this.data.item.lon==""?116.23:this.data.item.lon
    tt.openLocation({
      latitude:lat,
      longitude:lon,
      scale: 18,
      success() {
        console.log("打开地图成功");
      },
      fail(res) {
        console.log("打开地图失败:", res.errMsg);
      },
    });
  },
  getOrderInfo: function (uid) {
    let that = this;
    tt.request({
      url: globalUrls.mymyorderinfo + "?uid=" + uid,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        let datas = res.data.data
        that.setData({
          item: datas,
          marker: [{ "id": 1, "latitude": datas.lat, "longitude": datas.lon, "title": datas.storename }]
        })

      }, fail() {
        tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
      }
    })
  },
  call() {
    tt.makePhoneCall({
      phoneNumber: this.data.item.tel,
    });
  },
  saveImg: function () {
    tt.downloadFile({
      url: this.data.item.verificationcode,
      header: {
        "content-type": "application/json",
      },
      success: (res) => {
        let filePath = res.tempFilePath;
        tt.saveImageToPhotosAlbum({
          filePath,
          success: (res) => {
            tt.showToast({ title: "成功保存到本地相册" });
          },
          fail: (err) => {
          },
        });
      },
      fail: (err) => {
        tt.showModal({
          title: "下载出错",
          content: err.errMsg,
          showCancel: false,
        });
      },
    });
  },
  addcomment: function () {
    let that = this;
    let imgurl = []
    that.data.imgList.forEach((item, index) => {
      tt.uploadFile({
        url: "http://api.mayuntui.com/Base_Manage/Upload/UploadFileByForm",
        header: {
          "Content-Type": "multipart/form-data",
        },
        filePath: item,
        name: "file",
        method: "POST",
        dataType: "json",
        success(res) {
          let data = JSON.parse(res.data)
          imgurl.push({ "url": data.url })
          
          if (index+1 == that.data.imgList.length) {
            let ds={}
            if(imgurl.length!=0){
              ds={
                "orderuid": that.data.uid,
                "content": that.data.comment,
                "star": that.data.star,
                "img":imgurl
              }
            }else{
              ds={
                "orderuid": that.data.uid,
                "content": that.data.comment,
                "star": that.data.star
              }
            }
            tt.request({
              url: globalUrls.addcomment,
              method: "POST",
              header: { 'content-type': "application/json", "Authorization": "Bearer " + app.globalData.userInfo },
              data: ds,
              success: function (res) {
                if (res.data.success == false) {
                  tt.showToast({
                    title: res.data.msg,
                    icon: "none"
                  })
                  return;
                }
              }
            })
          }
        },
        fail() {
          wx.showToast({
            title: '服务器异常',
            icon: 'none'
          })
        }
      })

    })

  },
  ChooseImage() {
    wx.chooseImage({
      count: 4, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  setComment: function (e) {
    this.setData({
      comment: e.detail.value
    })
  },
  textareaBInput(e) {
    this.setData({
      textareaBValue: e.detail.value
    })
  }
})
