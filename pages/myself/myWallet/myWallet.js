import {
  globalUrls
} from "../../../utils/urls.js"
let app = getApp()
Page({
  data: {
    PageCur: 'basics',
    wallet:{},
    info:{},
    selected:0,
    discount:[],
    uid:""
  },
  onLoad: function (option) {
    this.setData({
      "info": app.globalData.info
    })
    let selected = option.selected;
    this.setData({
      selected: selected
    })
    this.getWallet()
    this.mycoupon()
  },
  mycoupon:function(){
    let that = this;
    tt.request({
      url: globalUrls.mycoupon + "?status=-2" ,
      method: "POST",
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      success: function (res) {
        if (res.data.success == false) {
          tt.showToast({
            title: '网络异常',
            icon: "error"
          })
          return;
        }
        let datas=res.data.data;
        for(let i=0;i<datas.length;i++){
          datas[i].end_time=datas[i].end_time.replace("T"," ");
          datas[i].start_time=datas[i].start_time.replace("T"," ");
        }
        that.setData({
          discount:datas,
        })
      }
    })
  },
  getWallet() { //0 全部刷新 1拼接
    let that = this;
    tt.request({
      url: globalUrls.mywallet,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
        if(res.data.success==false){
          tt.showToast({
            title: '服务器异常',
            icon:"error"
          })
          return;
        }
        that.setData({
          wallet:res.data.data
        })
      },fail(){
        tt.showToast({ title: '网络异常', icon: 'error', duration: 1000 })
      }
    })
  },
  tixian:function(){
    // tt.showToast({
    //   title: '请在抖讯云公众号公众号提现', // 内容
    //   icon:"none"
    // });
  },

  tabSelect(e) {
    this.setData({
      selected: e.currentTarget.dataset.id,
    })
  },
  goto(e){
    let uid=e.currentTarget.dataset.id
    this.setData({
      uid:uid
    })
  }
})