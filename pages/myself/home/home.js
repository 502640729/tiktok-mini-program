const {
  globalUrls
} = require("../../../utils/urls")
let app = getApp()
Component({
  data: {
    haveUserInfo: false,
    userInfo: {},
    servUrl: "",
    ct: 0,
    pct: 0,
    ColorList: app.globalData.ColorList,
    color:'red',
    item:[],
    count:[0,0,0,0],
    info:{},
    cs:0
  },
  lifetimes: {
    attached: function () {
      this.setData({
        "info": app.globalData.info
      })
      this.getItemList()
      this.getOrder()
      this.mycoupon();
    },
  },
  options: {
    addGlobalClass: true,
  },
  methods: {
    onLoad() {
      let that = this;
      setTimeout(function () {
        that.setData({
          loading: true
        })
      }, 500)
    },
    tixian:function(){
      return;
      tt.showToast({
        title: '通过抖讯公众号了解详情', // 内容
        icon:"none"
      });
    },
    call() {
      tt.makePhoneCall({
        phoneNumber: "4009675118",
        success(res) {
        },
      });
    },
    mycoupon:function(){
      let that = this;
      tt.request({
        url: globalUrls.mycoupon + "?status=-2" ,
        method: "POST",
        header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
        success: function (res) {
          if (res.data.success == false) {
            tt.showToast({
              title: '网络异常',
              icon: "error"
            })
            return;
          }

          that.setData({
            cs:res.data.data.length,
          })

        }
      })
    },
    getItemList() {
      let that = this;
      let data = "?type=2&pagesize=6&lon=" + app.globalData.location.jd + "&lat=" + app.globalData.location.wd + "&pageindex=1";
      tt.request({
        url: globalUrls.goodslist + data,
        header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
        method: "POST",
        success(res) {
          if(res.data.success==false){
            tt.showToast({
              title: '服务器异常',
              icon:"error"
            })
            return;
          }
          let datas=res.data.data
          for(let i=0;i<datas.length;i++){
            datas[i]["per"]=parseInt( (parseInt( datas[i].sales)/(parseInt(datas[i].sales)+parseInt(datas[i].stock)))*100)
          }
          console.log(datas)
          that.setData({
            item: datas,
          })
        }, fail() {
          tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
        }
      })
      
    },
    getUserInfo(xxx){
      let that = this;
      let data = "?nickName=" + xxx.nickName + "&avatarUrl=" + xxx.avatarUrl ;
      tt.request({
        url: globalUrls.setuserinfo+data,
        header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
        method: "POST",
        success(res) {
        }, fail() {
          tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
        }
      })
    },
    getOrder:function(){
      let that=this;
      tt.request({
        url: globalUrls.myorder+"?type=5",
        header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
        method: "POST",
        success(res) {
          let datas=res.data.data;
          let count=[datas.length,0,0]
          for(let i=0;i<datas.length;i++){
            if(datas[i].status==0)
              count[1]+=1;
            else if(datas[i].status==1)
              count[2]+=1;
          }
          that.setData({
            count:count
          })
        }, fail() {
          tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
        }
      })
    }
  }
})