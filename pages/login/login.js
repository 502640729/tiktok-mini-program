import { globalUrls } from "../../utils/urls.js"
let app = getApp()
Page({
  data: {
    haveInfo:true
  },
  onLoad: function (options) {
    let that=this
    
    app.userIdReadyCallback = res => {
      this.login()
      that.setData({
        haveInfo:app.globalData.haveInfo
      })
    }
  },
  login: function () {
   
    let that = this;
    tt.getSetting({
      success(res) {
        tt.authorize({
          scope: "scope.userInfo",
          success() {
            tt.getUserInfo({
              success: (res) => {
                app.globalData.info = res.userInfo
                that.setData({
                  userInfos:app.globalData.haveInfo
                })
                that.setUserInfo(res.userInfo)
                tt.switchTab({
                  url: '/pages/item/home/home' // 指定页面的url
                });
              },
            })
          }
        });
      },
    });
  },
  setUserInfo(xxx){
    let that = this;
    let data = "?nickName=" + xxx.nickName + "&avatarUrl=" + xxx.avatarUrl ;
    tt.request({
      url: globalUrls.setuserinfo+data,
      header: { 'content-type': globalUrls.defaultContent, "Authorization": "Bearer " + app.globalData.userInfo },
      method: "POST",
      success(res) {
      }, fail() {
        tt.showToast({ title: '服务器异常', icon: 'error', duration: 1000 })
      }
    })
  },
})